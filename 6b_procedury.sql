--przenosi pacjenta do tabeli archiwalnej
CREATE OR REPLACE PROCEDURE zarchiwizuj_dane_pacjenta(p_pacjent pacjent%ROWTYPE) AS
  c_nazwa_tabeli CONSTANT CHAR(12) := 'pacjent_arch';
BEGIN
  IF NOT czy_istnieje(c_nazwa_tabeli) THEN
    --przy tworzeniu tabeli nie mo�na u�ywa� typ�w zakotwiczonych
    EXECUTE IMMEDIATE '
      CREATE TABLE pacjent_arch (
        id            NUMBER (11) NOT NULL PRIMARY KEY,
        pacjent_id    NUMBER (11) NOT NULL ,
        imie          VARCHAR2 (20) NOT NULL ,
        nazwisko      VARCHAR2 (30) NOT NULL ,
        pesel         CHAR (11) ,
        ubezpieczenie CHAR (1) NOT NULL ,
        tel           CHAR(9),
        adres_id      NUMBER (11) ,
        data_zmiany   DATE DEFAULT SYSDATE NOT NULL
      )
    '; --dynamiczny sql
    EXECUTE IMMEDIATE 'CREATE SEQUENCE pacjent_arch_id_SEQ START WITH 1 NOCACHE ORDER' ;
    EXECUTE IMMEDIATE '
      CREATE OR REPLACE TRIGGER pacjent_arch_id_TRG BEFORE
        INSERT ON pacjent_arch FOR EACH ROW WHEN (NEW.id IS NULL) BEGIN :NEW.id := pacjent_arch_id_SEQ.NEXTVAL;
      END;
    '; --dynamiczny sql (WYMAGANE DODATKOWE UPRAWNIENIA - SZCZEGӣY W PLIKU "UWAGI.txt")
  END IF;
  --wywo�anie wprost powodowa�o b��d kompilacji spowodowany brakiem tabeli pacjent_arch (przy pierwszym uruchomieniu)
  --w ten spos�b omin��em walidacj� tego fragmentu
  EXECUTE IMMEDIATE 'INSERT INTO pacjent_arch(pacjent_id, imie, nazwisko, pesel, ubezpieczenie, adres_id) VALUES (
    :pacjent_id,
    :imie,
    :nazwisko,
    :pesel,
    :ubezpieczenie,
    :adres_id
  )' USING p_pacjent.id, p_pacjent.imie, p_pacjent.nazwisko, p_pacjent.pesel, p_pacjent.ubezpieczenie, 
    p_pacjent.adres_id; --parametry dynamicznego sql(typy zakotwiczone niedozwolone)
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury zarchiwizuj_dane_pacjenta
BEGIN
  IF czy_istnieje('pacjent_arch') THEN
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch istnieje');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch NIE istnieje');
  END IF;
END;
/

SELECT * FROM pacjent;
DECLARE
  v_pecjent pacjent%ROWTYPE;
BEGIN
  SELECT * INTO v_pecjent FROM pacjent WHERE id=1;
  zarchiwizuj_dane_pacjenta(v_pecjent);
END;
/
UPDATE pacjent SET nazwisko='AAABBBCCC' WHERE id=1;
SELECT * FROM pacjent;


BEGIN
  IF czy_istnieje('pacjent_arch') THEN
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch istnieje');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch NIE istnieje');
  END IF;
END;
/
SELECT * FROM pacjent_arch;



--wypisanie procentowego udzia�u m�czyzn i kobiet w�r�d pacjent�w
CREATE OR REPLACE PROCEDURE udzial_plci AS
  v_liczba_mezczyzn INT := 0;
  v_liczba_kobiet INT := 0;
  v_niezdefiniowano INT := 0;
  v_liczba_pacjentow INT;
  v_plec CHAR(1);
BEGIN
  SELECT count(id) INTO v_liczba_pacjentow FROM pacjent;
  FOR v_pacjent IN (SELECT pesel FROM pacjent) LOOP --niejawny kursor (bez bawienia si� z open-fetch-close i deklarowanie zmiennej)
    v_plec := jaka_plec(v_pacjent.pesel);
    IF v_plec = 'K' THEN
      v_liczba_kobiet := v_liczba_kobiet + 1;
    ELSIF v_plec = 'M' THEN
      v_liczba_mezczyzn := v_liczba_mezczyzn + 1;
    ELSE
      v_niezdefiniowano := v_niezdefiniowano + 1;
    END IF;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('KOBIET'||CHR(9)||'MʯCZYZN'||CHR(9)||'NIE OKRE�LONO');
  DBMS_OUTPUT.PUT_LINE('---------------------------------');
  DBMS_OUTPUT.PUT_LINE(RPAD(ROUND(v_liczba_kobiet*100/v_liczba_pacjentow, 2)||'%', 6, ' ')||CHR(9)||
    RPAD(ROUND(v_liczba_mezczyzn*100/v_liczba_pacjentow, 2)||'%', 9, ' ')||CHR(9)||
    ROUND(v_niezdefiniowano*100/v_liczba_pacjentow, 2)||'%');
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury udzial_plci
SELECT CASE 
  WHEN pesel IS NULL THEN 'brak' 
  ELSE pesel 
END AS pesel FROM pacjent;

EXECUTE udzial_plci;



--podnie� p�ace piel�gniarkom (np. ostatnie podwy�ki o 400 z� brutto)
--zwr�� najwy�sz� pensj� (podstaw� - bez dodatk�w) po podwy�kach - nieco na si��, ale przynajmniej co� innego
CREATE OR REPLACE PROCEDURE podwyzka_piel (p_podwyzka IN OUT NUMERIC) AS
  CURSOR cur_podwyzka IS SELECT * FROM stanowisko WHERE nazwa LIKE 'piel�gniarka%' FOR UPDATE;
  v_stanowisko stanowisko%ROWTYPE;
BEGIN
  OPEN cur_podwyzka; --dla wprawy tym razem wprost
  LOOP
    FETCH cur_podwyzka INTO v_stanowisko;
    EXIT WHEN cur_podwyzka%NOTFOUND;
    UPDATE stanowisko 
    SET podstawa = podstawa+p_podwyzka
    WHERE CURRENT OF cur_podwyzka;
  END LOOP;
  CLOSE cur_podwyzka;
  SELECT MAX(podstawa) INTO p_podwyzka FROM (SELECT * FROM stanowisko WHERE nazwa LIKE 'piel�gniarka%');
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury podwyzka_piel
SELECT nazwa, podstawa FROM stanowisko;
SELECT pwz, pensja FROM zarobki_piel;
DECLARE
  v_param stanowisko.podstawa%TYPE := 400;
BEGIN
  podwyzka_piel(v_param);
  DBMS_OUTPUT.PUT_LINE('Po podwy�ce, najwy�sza podstawa wynosi: '||v_param);
END;
/
SELECT nazwa, podstawa FROM stanowisko;
SELECT pwz, pensja FROM zarobki_piel;



--zmniejsza ilo�� danej szczepionki o 1 sztuk�
--zwraca komunikat o ilo�ci pozosta�ych lub komunikat o wyczerpaniu si� zapas�w szczepionki
CREATE OR REPLACE PROCEDURE odejmij_szczepionke (p_nazwa IN szczepionka.nazwa%TYPE, p_komunikat OUT VARCHAR2) AS
  v_ilosc szczepionka.ilosc%TYPE;
BEGIN
  SELECT ilosc INTO v_ilosc FROM szczepionka WHERE nazwa = p_nazwa;
  IF v_ilosc > 0 THEN
    UPDATE szczepionka SET ilosc = ilosc-1 WHERE nazwa = p_nazwa;
    p_komunikat := 'Pozosta�o '||(v_ilosc-1)||' sztuk';
  ELSE
    p_komunikat := 'Dana szczepionka ju� si� sko�czy�a';
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury podwyzka_piel
DECLARE
  v_komunikat VARCHAR2(50);
BEGIN
  odejmij_szczepionke('Meningo A + C', v_komunikat);
  DBMS_OUTPUT.PUT_LINE(v_komunikat);
END;
/



--wypisuje X pacjent�w, kt�rzy mieli najwi�cej wizyt (a czekanie w kolejkach ukochali jeszcze za dawnego ustroju)
--wykorzystuje informacj� o p�ci pacjenta
CREATE OR REPLACE PROCEDURE wladcy_kolejek (p_ilu INT) AS
  v_tytul VARCHAR(5);
BEGIN
  FOR v_ancymon IN (
    SELECT nazwisko, imie, pesel, wizyt 
    FROM (
      SELECT p.id, nazwisko, imie, pesel, COUNT(p.id) AS wizyt 
      FROM wizyta w 
      JOIN pacjent p 
      ON w.pacjent_id = p.id 
      GROUP BY p.id, nazwisko, imie, pesel 
      ORDER BY wizyt DESC
    ) WHERE ROWNUM <= p_ilu) LOOP
      IF jaka_plec(v_ancymon.pesel) = 'K' THEN
        v_tytul := 'Pani ';
      ELSIF jaka_plec(v_ancymon.pesel) = 'M' THEN
        v_tytul := 'Pan ';
      ELSE
        v_tytul := '';
      END IF;
      DBMS_OUTPUT.PUT_LINE(v_tytul||v_ancymon.nazwisko||' '||v_ancymon.imie||': '||v_ancymon.wizyt||' wizyt');
    END LOOP;
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury wladcy_kolejek
EXECUTE wladcy_kolejek(1);
EXECUTE wladcy_kolejek(4);