SET SERVEROUTPUT ON

CREATE OR REPLACE VIEW zarobki_lekarz AS
SELECT nazwisko, imie, pwz, nrb AS "NUMER RACHUNKU BANKOWEGO", "SUMA PODSTAW", dodatek, pensja
FROM (SELECT l.nazwisko, l.imie, l.pwz, l.nrb, sum(s.podstawa*ls.czesc_etatu) as "SUMA PODSTAW", 
        l.dodatek as dodatek, sum(s.podstawa*ls.czesc_etatu)+l.dodatek as pensja 
      FROM lekarz l
      INNER JOIN lekarz_stan ls
      ON l.id = ls.lekarz_id
      INNER JOIN stanowisko s
      ON s.id = ls.stanowisko_id
      GROUP BY l.nazwisko, l.imie, l.pwz, l.nrb, l.dodatek)
ORDER BY nazwisko, imie, pensja;


--ile wykonanych szczepie� przypada na 100 zarejestrowanych pacjent�w
CREATE OR REPLACE VIEW czestosc_szczep AS
SELECT TO_CHAR(stosunek, '999.99') AS "LICZBA SZCZEPIE� NA 100 PACJ."
FROM (
  SELECT (l_szczepien/l_pacjentow)*100 AS stosunek
  FROM (
    SELECT 
      (SELECT COUNT(*) FROM pacjent) l_pacjentow,
      (SELECT COUNT(z.id) 
        FROM zabieg z 
        JOIN skierzabieg s 
        ON z.skierzabieg_id = s.id 
        JOIN wizyta w 
        ON s.wizyta_id = w.id 
        WHERE s.typzabiegu_id = (
          SELECT id 
          FROM typzabiegu 
          WHERE nazwa = 'szczepienie'
        )
      ) l_szczepien
      FROM dual
  )
);


--dodatkowy (prosty)
CREATE OR REPLACE VIEW zarobki_piel AS
SELECT nazwisko, imie, pwz, nrb, podstawa, dodatek, podstawa + dodatek as pensja
FROM pielegniarka
INNER JOIN stanowisko s
ON stanowisko_id = s.id
GROUP BY nazwisko, imie, pwz, nrb, podstawa, dodatek
ORDER BY nazwisko, imie, pensja;


--pacjent z pe�nym adresem i wojew�dztwem
CREATE OR REPLACE VIEW pacjent_z_adresem AS
SELECT nazwisko, imie, pesel, 
  CASE ubezpieczenie
    WHEN '0' THEN 'nie'
    ELSE 'tak'
  END AS ubezpieczenie, 
  tel, ulica, nr_domu, klatka, nr_mieszkania, kod, miejscowosc, nazwa as wojewodztwo
FROM pacjent p 
JOIN adres a 
ON p.adres_id = a.id 
JOIN wojewodztwo w 
ON a.wojewodztwo_id = w.id 
ORDER BY nazwisko, imie;



--prezentacja
SELECT * FROM zarobki_lekarz;
SELECT * FROM zarobki_piel;
SELECT * FROM czestosc_szczep;
SELECT * FROM pacjent_z_adresem;