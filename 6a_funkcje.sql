--funkcja sprawdzaj�ca, czy pesel zawiera wy��cznie cyfry (funkcja pomocnicza dla spr_pesel)
CREATE OR REPLACE FUNCTION isnumeric( p_napis CHAR)
  RETURN BOOLEAN
AS
  l NUMBER;
BEGIN
  IF INSTR(p_napis, ' ') != 0 OR p_napis IS NULL THEN
    RETURN FALSE;
  END IF;
  l := TO_NUMBER(p_napis);
  RETURN TRUE;
EXCEPTION
  WHEN value_error
  THEN
    RETURN FALSE;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji isnumeric
DECLARE
   TYPE napis IS TABLE OF VARCHAR2(20);
   napisy napis := napis('1234', '123a', 'abcd', NULL);
   i PLS_INTEGER;
BEGIN
   FOR i in napisy.FIRST..napisy.LAST LOOP
    IF isnumeric(napisy(i)) THEN
      DBMS_OUTPUT.PUT_LINE(napisy(i)||' to liczba');
    ELSE
      DBMS_OUTPUT.PUT_LINE(napisy(i)||' to napis');
    END IF;
   END LOOP;
END;
/



--funkcja waliduj�ca poprawno�� numeru PESEL
CREATE OR REPLACE FUNCTION spr_pesel (p_pesel CHAR)
RETURN BOOLEAN AS
  e_bledny_pesel EXCEPTION;
  TYPE waga IS TABLE OF NUMERIC(1);
  wagi waga := waga(1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1);
  pozycja PLS_INTEGER;
  wynik NUMBER := 0;
BEGIN
  IF p_pesel IS NULL THEN RETURN TRUE; END IF; --obcokrajowcy (funkcja b�dzie u�yta w wyzwalaczu, wi�c pusty musi by� poprawny
    IF (LENGTH(p_pesel) = 11 AND isnumeric(p_pesel)) THEN
    FOR pozycja in 1..11 LOOP
      wynik := wynik + TO_NUMBER(SUBSTR(p_pesel, pozycja, 1)) * wagi(pozycja);
    END LOOP;
    IF (MOD(wynik, 10) = 0) THEN
      RETURN TRUE;
    ELSE
       RAISE e_bledny_pesel;
    END IF;
  ELSE
    RAISE e_bledny_pesel;  --tak, dodany na si��
  END IF;
  EXCEPTION
    WHEN e_bledny_pesel THEN
      RETURN FALSE;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji spr_pesel
DECLARE
  TYPE pesel IS TABLE OF VARCHAR2(20);
  pesele pesel := pesel(
    '123456789012', --niepoprawny(za d�ugi)
    '123456789',    --niepoprawny (za kr�tki)
    '12345678abc',  --niepoprawny (napis)
    '12345678901',  --niepoprawny
    '47020409998',  --poprawny
    '73031613189',  --poprawny
    NULL,           --poprawny (obcokrajowcy)
    '07232510042',  --poprawny
    '63011108697',  --poprawny
    '23093012818'   --niepoprawny
  );
  i PLS_INTEGER;
BEGIN
   FOR i in pesele.FIRST..pesele.LAST LOOP
    IF spr_pesel(pesele(i)) THEN
      DBMS_OUTPUT.PUT_LINE(pesele(i)||' jest poprawny');
    ELSE
      DBMS_OUTPUT.PUT_LINE(pesele(i)||' jest niepoprawny');
    END IF;
   END LOOP;
END;
/



--sprawdza, czy tabela istnieje (funkcja potrzebna do procedury wypisywania pacjenta)
--NALE�Y PAMI�TA� O NADANIU UPRAWNIE� (OPIS W PLIKU "UWAGI.txt")
CREATE OR REPLACE FUNCTION czy_istnieje(p_nazwa_tabeli VARCHAR2)
RETURN BOOLEAN AS
  v_count INT;
BEGIN
  SELECT liczba INTO v_count 
  FROM (
    SELECT COUNT(table_name) AS liczba FROM sys.dba_tables WHERE table_name=UPPER(p_nazwa_tabeli)
  );

  IF v_count = 1 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji czy_istnieje
BEGIN
  IF czy_istnieje('pacjent') THEN
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent istnieje');
  END IF;
  IF NOT czy_istnieje('aoiwerpcwiour') THEN
    DBMS_OUTPUT.PUT_LINE('Podana tabela nie istnieje');
  END IF;
END;
/



--zwraca plec na podstawie nr pesel
CREATE OR REPLACE FUNCTION jaka_plec(p_pesel pacjent.pesel%TYPE)
RETURN CHAR AS
BEGIN
  IF p_pesel IS NULL THEN
    RETURN '?';
  ELSIF MOD(TO_NUMBER(SUBSTR(p_pesel, 10, 1)), 2) = 0 THEN
    RETURN 'K';
  ELSE
      RETURN 'M';
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji jaka_plec
BEGIN
  DBMS_OUTPUT.PUT_LINE('69062414266 => '||jaka_plec('69062414266'));
  DBMS_OUTPUT.PUT_LINE('97081906259 => '||jaka_plec('97081906259'));
  DBMS_OUTPUT.PUT_LINE('brak        => '||jaka_plec(NULL));
END;
/
--deklaruj� dopiero tutaj, bo by�a mi potrzebna funkcja jaka_plec
CREATE VIEW udzial_plci_view AS
SELECT 
  CASE jaka_plec(pesel) 
    WHEN 'K' THEN 'Kobiety' 
    WHEN 'M' THEN 'M�czy�ni' 
    ELSE 'Nieokre�lono' 
  END AS P�e�, 
  COUNT(jaka_plec(pesel)) AS Liczba  
FROM pacjent GROUP BY jaka_plec(pesel);



--zwraca dat� urodzenia na podstawie nr pesel
CREATE OR REPLACE FUNCTION data_urodzenia(p_pesel pacjent.pesel%TYPE)
RETURN DATE AS
  v_dzien INT;
  v_miesiac INT;
  v_rok INT;
BEGIN
  IF p_pesel IS NULL OR NOT spr_pesel(p_pesel) THEN
    RAISE_APPLICATION_ERROR(-20000, 'Nie mo�na wyci�gn�� daty urodzenia z pustego lub niepoprawnego numeru PESEL');
  ELSE 
    v_rok := SUBSTR(p_pesel, 1, 2);
    v_miesiac := SUBSTR(p_pesel, 3, 2);
    v_dzien := SUBSTR(p_pesel, 5, 2);
    CASE 
      WHEN v_miesiac > 80 THEN v_rok := 1800 + v_rok; v_miesiac := v_miesiac - 80;
      WHEN v_miesiac > 20 THEN v_rok := 2000 + v_rok; v_miesiac := v_miesiac - 20;
      ELSE v_rok := 1900 + v_rok;
    END CASE;
    RETURN TO_DATE(v_rok||'/'||v_miesiac||'/'||v_dzien, 'yyyy/mm/dd');
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji data_urodzenia
BEGIN
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL 12920793018 urodzi�a si�: '||TO_CHAR(data_urodzenia('12920793018'), 'dd-mm-yyyy'));
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL 81040714542 urodzi�a si�: '||TO_CHAR(data_urodzenia('81040714542'), 'dd-mm-yyyy'));
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL 03250810988 urodzi�a si�: '||TO_CHAR(data_urodzenia('03250810988'), 'dd-mm-yyyy'));
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL NULL urodzi�a si�: '||data_urodzenia(NULL)); --b��d
END;
/