INSERT INTO wojewodztwo (nazwa) VALUES ('dolno�l�skie');
INSERT INTO wojewodztwo (nazwa) VALUES ('kujawsko-pomorskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('lubelskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('lubuskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('��dzkie');
INSERT INTO wojewodztwo (nazwa) VALUES ('ma�opolskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('mazowieckie');
INSERT INTO wojewodztwo (nazwa) VALUES ('opolskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('podkarpackie');
INSERT INTO wojewodztwo (nazwa) VALUES ('podlaskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('pomorskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('�l�skie');
INSERT INTO wojewodztwo (nazwa) VALUES ('�wi�tokrzyskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('warmi�sko-mazurskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('wielkopolskie');
INSERT INTO wojewodztwo (nazwa) VALUES ('zachodniopomorskie');


-- korzystamy z tego, �e domy�lna miejscowo�� to Warszawa, a wojew�dztwo to mazowieckie
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('�wi�tokrzyska', 31, 'K', 12, '00-001');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('�wi�tokrzyska', 20, 'B', 25, '00-002');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('�wi�tokrzyska', 20, 'E', 6, '00-002');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Jasna', 17, 'A', 43, '00-003');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Jasna', 17, 'A', 24, '00-003');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Jasna', 17, 'H', 12, '00-003');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Marsza�kowska', 136, 'F', 1, '00-004');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Rysia', 1, 'D', 3, '00-005');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Szkolna',	88, 'D', 8, '00-006');
INSERT INTO adres (ulica, nr_domu, klatka, nr_mieszkania, kod) VALUES ('Szkolna',	88, 'D', 9, '00-006');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Jasna', 5, '00-007');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Marsza�kowska', 124, '00-008');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Moniuszki Stanis�awa', 6, '00-009');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('M�ynarskiego Emila', 54, '00-009');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Sienkiewicza Henryka', 8, '00-010');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Moniuszki Stanis�awa', 1, '00-014');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Sienkiewicza Henryka', 2, '00-015');
INSERT INTO adres (ulica, nr_domu, kod) VALUES ('Z�ota', 10, '00-019');


-- korzystamy z tego, �e domy�lnie pacjent jest ubezpieczony
INSERT INTO pacjent (imie, nazwisko, pesel, tel, adres_id) VALUES ('Jakub', 'Kowalski', '97081906259', '795185938', 1);
INSERT INTO pacjent (imie, nazwisko, pesel, tel, adres_id) VALUES ('Aleksandra', 'Kowalska', '69062414266', '797729685', 1);
INSERT INTO pacjent (imie, nazwisko, pesel, tel, adres_id) VALUES ('Dominik', 'Kowalski', '86060818175', '722307372', 1);
INSERT INTO pacjent (imie, nazwisko, pesel, tel, adres_id) VALUES ('Maria', 'Smoli�ska', '39090300642', '607374802', 2);
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Marcel', 'Smoli�ski', '41031304918', 2);
INSERT INTO pacjent (imie, nazwisko) VALUES ('Urlich', 'von Jungingen'); --obcokrajowiec - bez adresu i numeru PESEL
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Zuzanna', 'Adamczyk', '32012206826', 4);
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Nikola', 'Augustyniak', '39060605041', 5);
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Kamila', 'Augustyniak', '71032803000', 5);
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Robert', 'Szulc', '85122703491', 6);
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Gabriela', 'Witkowska', '75061901625', 7);
INSERT INTO pacjent (imie, nazwisko, pesel, adres_id) VALUES ('Anna', 'Skowro�ska', '83021306803', 8);


INSERT INTO stanowisko (nazwa, podstawa) VALUES ('piel�gniarka', 2000);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('piel�gniarka zabiegowa', 2200);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('piel�gniarka szczepienna', 2300);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('piel�gniarka �rodowiskowa', 2300);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('lekarz rodzinny', 3800);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('lekarz pediatra', 4000);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('lekarz medycyny pracy', 4000);
INSERT INTO stanowisko (nazwa, podstawa) VALUES ('lekarz kardiolog', 4400);


INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Paulina', 'Sobczak', '84022708687', '0352165P', 0, '64322431905507831309931262', 9, 1);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Irena', 'W�jcik', '74062107665', '0718010P', 0, '28622340104088137826830612', 10, 1);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Ma�gorzata', 'Jaworska', '64020409421', '1888283P', 100, '28388688877442197630390443', 11, 2);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Ewa', 'Szulc', '74110800360', '1812565P', 100, '61008812896281753131214105', 12, 2);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Jadwiga', 'Brzezi�ska', '71121517542', '0459448P', 150, '75304483981151541817318436', 13, 2);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Joanna', 'Baranowska', '49122319449', '4237328P', 50, '19904150694366076491638780', 14, 3);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Gra�yna', 'G�rska', '59101516905', '1549686P', 200, '26940326051692625685568757', 15, 3);
INSERT INTO pielegniarka (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id, stanowisko_id) 
  VALUES ('Wanda', 'Krawczyk', '77100916229', '0664135P', 50, '37420883172349753647360661', 16, 4);
  

INSERT INTO lekarz (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id) 
	VALUES ('Kazimiera', 'Zalewska', '68060309228', '9396201', 150, '10860587477954477385240146', 14);
INSERT INTO lekarz (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id) 
	VALUES ('Sylwia', 'Ka�mierczak', '76061510381', '8549241', 0, '18033297827981917410824953', 15);
INSERT INTO lekarz (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id) 
	VALUES ('Jacek', 'Laskowski', '77122115055', '3156415', 100, '45305294219564648216896930', 16);
INSERT INTO lekarz (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id) 
	VALUES ('Micha�', 'Sobczak', '83100612010', '2173721', 400, '06797702393769935872040480', 17);
INSERT INTO lekarz (imie, nazwisko, pesel, pwz, dodatek, nrb, adres_id) 
	VALUES ('Micha�', 'Sobczak', '65082503591', '6672254', 250, '72983625299610555845190237', 18);
  
  
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (1, 5, 0.5);
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (1, 6, 0.5);
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (2, 5, 1);
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (2, 7, 0.25);
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (3, 8, 0.75);
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (4, 5, 1);
INSERT INTO lekarz_stan (lekarz_id, stanowisko_id, czesc_etatu) VALUES (5, 6, 1.2);


INSERT INTO specjalizacja (nazwa) VALUES ('lekarz rodzinny');
INSERT INTO specjalizacja (nazwa) VALUES ('pediatra');
INSERT INTO specjalizacja (nazwa) VALUES ('medycyna pracy');
INSERT INTO specjalizacja (nazwa) VALUES ('kardiolog');
INSERT INTO specjalizacja (nazwa) VALUES ('diabetolog');
INSERT INTO specjalizacja (nazwa) VALUES ('alergolog');
INSERT INTO specjalizacja (nazwa) VALUES ('endokrynolog');
INSERT INTO specjalizacja (nazwa) VALUES ('hematolog');
INSERT INTO specjalizacja (nazwa) VALUES ('neurolog');
INSERT INTO specjalizacja (nazwa) VALUES ('reumatolog');


INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (1, 1);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (1, 2);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (2, 1);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (2, 3);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (3, 4);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (4, 1);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (5, 2);
INSERT INTO lekarz_spec (lekarz_id, specjalizacja_id) VALUES (5, 5);


INSERT INTO jednchor (kod, nazwa) VALUES ('A28.9', 'Bakteryjna choroba odzwierz�ca, nie okre�lona');
INSERT INTO jednchor (kod, nazwa) VALUES ('A05.9', 'Bakteryjne zatrucie pokarmowe, nie okre�lone');
INSERT INTO jednchor (kod, nazwa) VALUES ('A04.9', 'Zaka�enie bakteryjne jelit, nie okre�lone');
INSERT INTO jednchor (kod, nazwa) VALUES ('B30', 'Wirusowe zapalenie spoj�wek');
INSERT INTO jednchor (kod, nazwa) VALUES ('B30.2', 'Wirusowe zapalenie gard�a i spoj�wek');
INSERT INTO jednchor (kod, nazwa) VALUES ('C03', 'Nowotw�r z�o�liwy dzi�s�a');
INSERT INTO jednchor (kod, nazwa) VALUES ('D03.1', 'Czerniak in situ powieki ��cznie z k�tem oka');
INSERT INTO jednchor (kod, nazwa) VALUES ('E03.4', 'Zanik tarczycy (nabyty)');
INSERT INTO jednchor (kod, nazwa) VALUES ('E06.1', 'Zapalenie tarczycy podostre');
INSERT INTO jednchor (kod, nazwa) VALUES ('F01.1', 'Ot�pienie wielozawa�owe');
INSERT INTO jednchor (kod, nazwa) VALUES ('F06.0', 'Halucynoza organiczna');
INSERT INTO jednchor (kod, nazwa) VALUES ('H01.0', 'Zapalenia brzeg�w powiek');
INSERT INTO jednchor (kod, nazwa) VALUES ('I83.1', '�ylaki ko�czyn dolnych z zapaleniem');
INSERT INTO jednchor (kod, nazwa) VALUES ('J00', 'Ostre zapalenie nosa i gard�a (przezi�bienie)');
INSERT INTO jednchor (kod, nazwa) VALUES ('K00.4', 'Zaburzenia rozwoju z�ba');
INSERT INTO jednchor (kod, nazwa) VALUES ('K02.4', 'Odontoklazja');
INSERT INTO jednchor (kod, nazwa) VALUES ('L03', 'Zapalenie tkanki ��cznej');
INSERT INTO jednchor (kod, nazwa) VALUES ('L12.0', 'Pemfigoid p�cherzowy');
INSERT INTO jednchor (kod, nazwa) VALUES ('L20', 'Atopowe zapalenie sk�ry');
INSERT INTO jednchor (kod, nazwa) VALUES ('L21', '�ojotokowe zapalenie sk�ry');


INSERT INTO jednostka (nazwa) VALUES ('ml');
INSERT INTO jednostka (nazwa) VALUES ('g');
INSERT INTO jednostka (nazwa) VALUES ('mg');
INSERT INTO jednostka (nazwa) VALUES ('tab.');
INSERT INTO jednostka (nazwa) VALUES ('amp.');
INSERT INTO jednostka (nazwa) VALUES ('j.m.');
INSERT INTO jednostka (nazwa) VALUES ('%'); 
INSERT INTO jednostka (nazwa) VALUES ('mg/ml');
INSERT INTO jednostka (nazwa) VALUES ('mg/g');


INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('AUGMENTIN', 5, 5, 600, 3);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('EXACYL', 5, 5, 100, 8);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('NOOTROPIL', 30, 4, 200, 8);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('NOOTROPIL', 60, 4, 200, 8);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id) VALUES ('VOLTAREN', 50, 2);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('ZYRTEC', 20, 5, 10, 1);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('ZYRTEC', 20, 4, 10, 3);
INSERT INTO lek (nazwa, wielkosc, wielkosc_jedn_id, dawka, dawka_jedn_id) VALUES ('ZYRTEC', 20, 4, 20, 3);


INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (1, 1);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (2, 2);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (2, 3);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (3, 4);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (3, 5);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (4, 6);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (5, 7);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (6, 8);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (7, 9);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (8, 10);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (8, 11);
INSERT INTO lek_naco (lek_id, jednchor_id) VALUES (8, 12);


INSERT INTO lek_przeciwsk (lek_id, jednchor_id) VALUES (1, 13);
INSERT INTO lek_przeciwsk (lek_id, jednchor_id) VALUES (1, 14);
INSERT INTO lek_przeciwsk (lek_id, jednchor_id) VALUES (2, 13);
INSERT INTO lek_przeciwsk (lek_id, jednchor_id) VALUES (3, 13);
INSERT INTO lek_przeciwsk (lek_id, jednchor_id) VALUES (6, 18);
INSERT INTO lek_przeciwsk (lek_id, jednchor_id) VALUES (8, 19);


INSERT INTO podanie (sposob) VALUES ('doustnie');
INSERT INTO podanie (sposob) VALUES ('podsk�rnie');
INSERT INTO podanie (sposob) VALUES ('�r�dsk�rnie');
INSERT INTO podanie (sposob) VALUES ('domi�niowo');


INSERT INTO szczepionka (nazwa, sklad, ilosc, podanie_id) VALUES ('Act-HIB', 
  'polisacharyd otoczkowy Haemophilus influenzae typu b skoniugowany z toksoidem t�cowym', 20, 4);
INSERT INTO szczepionka (nazwa, ilosc, podanie_id) VALUES ('Agrippal', 15, 4);
INSERT INTO szczepionka (nazwa, ilosc, podanie_id) VALUES ('Boostrix', 34, 2);
INSERT INTO szczepionka (nazwa, sklad, ilosc, podanie_id) VALUES ('Clodivac', 'toksoid t�cowy i toksoid b�oniczy', 18, 1);
INSERT INTO szczepionka (nazwa, ilosc, podanie_id) VALUES ('Engerix B', 11, 2);
INSERT INTO szczepionka (nazwa, sklad, ilosc, podanie_id) VALUES ('Havrix', '1440 j. ELISA wirusa zapalenia w�troby typu A', 21, 3);
INSERT INTO szczepionka (nazwa, ilosc, podanie_id) VALUES ('Infanrix - DTPa', 20, 4);
INSERT INTO szczepionka (nazwa, ilosc, podanie_id) VALUES ('Meningo A + C', 14, 3);
INSERT INTO szczepionka (nazwa, sklad, ilosc, podanie_id) VALUES ('Prevenar 13', 'Streptococcus pneumoniae', 19, 4);


INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (1, 15);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (2, 6);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (3, 2);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (3, 8);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (4, 9);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (5, 11);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (6, 17);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (7, 11);
INSERT INTO szczep_naco (szczepionka_id, jednchor_id) VALUES (8, 1);


INSERT INTO szczep_przeciwsk (szczepionka_id, jednchor_id) VALUES (1, 14);
INSERT INTO szczep_przeciwsk (szczepionka_id, jednchor_id) VALUES (3, 1);
INSERT INTO szczep_przeciwsk (szczepionka_id, jednchor_id) VALUES (3, 11);
INSERT INTO szczep_przeciwsk (szczepionka_id, jednchor_id) VALUES (3, 20);
INSERT INTO szczep_przeciwsk (szczepionka_id, jednchor_id) VALUES (7, 20);


INSERT INTO typgabinetu (nazwa) VALUES ('lekarski');
INSERT INTO typgabinetu (nazwa) VALUES ('pediatryczny');
INSERT INTO typgabinetu (nazwa) VALUES ('zabiegowy');
INSERT INTO typgabinetu (nazwa) VALUES ('szczepie�');


INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('2A', 1);
INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('2B', 2);
INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('6.1', 1);
INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('6.2', 1);
INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('7', 1);
INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('9.2C', 3);
INSERT INTO gabinet (numer, typgabinetu_id) VALUES ('SZ10', 4);
  

INSERT INTO typwizyty (nazwa) VALUES ('diagnostyczna');
INSERT INTO typwizyty (nazwa) VALUES ('konsultacyjna');
INSERT INTO typwizyty (nazwa) VALUES ('kontrolna');


INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-05', 'b�l ucha i brzucha', 5, 1, 1, 1);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-08', 'pacjent skar�y si� na sytuacj� polityczn�', 2, 1, 2, 1);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-08', 'utrata czucia w nogach', 2, 6, 1, 3);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-12', 'pacjent przyszed� pogada� o �yciu s�siadki', 4, 5, 1, 5);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-13', 'wysoka gor�czka od ponad 48h', 1, 7, 3, 4);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-15', 'pacjent ma zatkany nos', 1, 12, 1, 1);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-16', 'przezi�bienie i dreszcze', 1, 12, 2, 1);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-21', 'pacjent z�ama� nog� podczas sianokos�w (w grudniu)', 2, 6, 3, 5);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-22', 'nasilaj�ce si� b�le reumatyczne', 2, 9, 2, 1);
INSERT INTO wizyta (data, opis, lekarz_id, pacjent_id, typwizyty_id, gabinet_id) VALUES (
  '2016-12-22', 'b�l gard�a spowodowany prowadzeniem zaj�� w szkole', 2, 11, 2, 1);


INSERT INTO skierowanie (numer, opis, specjalizacja_id, wizyta_id) VALUES (
  '000000000010345067080434', 'prosz� spr. czy pacj. nie ma alergii na kurz', 6, 3);
INSERT INTO skierowanie (numer, opis, specjalizacja_id, wizyta_id) VALUES (
  '000000000010345067080435', 'podejrzenie ucisku na nerw', 9, 3);
INSERT INTO skierowanie (numer, opis, specjalizacja_id, wizyta_id) VALUES (
  '000000000010345067080436', 'd�ugotrwa�e b�le reumatyczne', 10, 9);


INSERT INTO diagnoza (wizyta_id, jednchor_id, zalecenia) VALUES (3, 11, 'ograniczy� spo�ycie alkoholu');
INSERT INTO diagnoza (wizyta_id, jednchor_id, zalecenia) VALUES (4, 19, 'ma�� stosowa� przez 2 tygodnie');
INSERT INTO diagnoza (wizyta_id, jednchor_id, zalecenia) VALUES (5, 3, 'g�owa do g�ry');
INSERT INTO diagnoza (wizyta_id, jednchor_id, zalecenia) VALUES (5, 12, 'przyj�� za tydzie� do kontroli');
INSERT INTO diagnoza (wizyta_id, jednchor_id, zalecenia) VALUES (9, 13, 'okada� bol�ce miejsca termoforem');


INSERT INTO recepta (numer, wizyta_id) VALUES ('100000000003450934853475', 4);
INSERT INTO recepta (numer, wizyta_id) VALUES ('100000000003450934853476', 5);
INSERT INTO recepta (numer, wizyta_id) VALUES ('100000000003450934853477', 5);
INSERT INTO recepta (numer, wizyta_id) VALUES ('100000000003450934853478', 9);


INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (1, 3, 100);
INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (1, 4, 75);
INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (2, 2, 50);
INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (3, 2, 30);
INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (4, 7, 30);
INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (4, 1, 100);
INSERT INTO rec_lek (recepta_id, lek_id, odplatnosc) VALUES (4, 8, 100);


INSERT INTO typzabiegu (nazwa) VALUES ('szczepienie');
INSERT INTO typzabiegu (nazwa) VALUES ('pobranie krwi');
INSERT INTO typzabiegu (nazwa) VALUES ('zmiana opatrunku');
INSERT INTO typzabiegu (nazwa) VALUES ('injekcja');


INSERT INTO skierzabieg (numer, uwagi, wizyta_id, typzabiegu_id, szczepionka_id) 
  VALUES ('200000000000234098234058', 'prosz� wzi�� najgrubsz� ig��', 1, 1, 5);
INSERT INTO skierzabieg (numer, wizyta_id, typzabiegu_id) 
  VALUES ('200000000000234098234059', 5, 2);
INSERT INTO skierzabieg (numer, wizyta_id, typzabiegu_id) 
  VALUES ('200000000000234098234060', 8, 3);


INSERT INTO zabieg (skierzabieg_id, data, uwagi, pielegniarka_id, pacjent_id, gabinet_id) 
  VALUES (1, '2016-12-05', 'najwi�ksza ig�a - pacjent zemdla� na sam widok', 3, 1, 7);
INSERT INTO zabieg (skierzabieg_id, data, pielegniarka_id, pacjent_id, gabinet_id)
  VALUES (2, '2016-12-17', 5, 7, 6);
INSERT INTO zabieg (skierzabieg_id, data, pielegniarka_id, pacjent_id, gabinet_id)
  VALUES (3, '2016-12-22', 3, 6, 6);